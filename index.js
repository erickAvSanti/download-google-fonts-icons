const fs = require('fs');
const path = require('path');
const https = require('https');
const data = JSON.parse(fs.readFileSync('icons.json', 'utf8'));
const mapping = {};

function download2(icon_text, version){
  return new Promise(async (resolve, reject) => {
    if(version <=0) {
      reject(`invalid version, version=${version}`);
      return;
    }
    if(mapping[`${version}_${icon_text}`]){
      reject(`already downloaded, version=${version}, icon_text=${icon_text}`);
      return;
    }
    mapping[`${version}_${icon_text}`] = 1;
    const url = `https://fonts.gstatic.com/s/i/materialiconsoutlined/${icon_text}/v${version}/24px.svg`;
    const filepath = path.join(__dirname, `icons/${icon_text}.svg`);
    if(fs.existsSync(filepath)){
      reject(`file exists = ${filepath}`);
      return;
    }
    https.get(url, (res) => {
      const { statusCode } = res;
      if(statusCode === 200){
        const file = fs.createWriteStream(filepath);
        res.pipe(file)
        file.on('finish', () => {
          file.close();
          resolve(file);
        });
        file.on('error', () => {
          fs.unlink(filepath);
          reject(`cannot save file, file_path=${filepath}`);
        });
      }else{
        resolve(`https get status not 200, status = ${statusCode}`);
      }
    }).on('error', (e) => {
      reject({
        is_http_error: true,
        message: 'http error',
        error: e,
      });
    });
  });
}


async function run(index = 0){
  console.log(`run: index = ${index}`);
  if(index >= data.length){
    console.log(`exiting from run`);
    return;
  }
  const icon_text = data[index];
  console.log(`procesando icon_text: ${icon_text}, index = ${index}, of = ${data.length}`);
  try {
    await downloadOneVersion(icon_text, 5);
    console.log(`ending downloadOneVersion`);
    setTimeout(async () => {
      console.log(`re-running for =${index + 1}`);
      await run(index + 1);
    }, 1);
  } catch (error) {
    console.log(error);
  }
}
async function downloadOneVersion(icon_text, version){
  console.log(`downloadOneVersion: icon_text = ${icon_text}, version = ${version}`);
  if(version <= 0) {
    console.log(`version <= 0 = ${version}`);
    return;
  }
  try {
    const result = await download2(icon_text, version);
    console.log(`finish: icon_text = ${icon_text}, version = ${version}`);
    setTimeout(async () => {
      await downloadOneVersion(icon_text, version - 1);
    } , 1);
  } catch (error) {
    console.error(error);
  }
}
function sleep(ms) {
  return new Promise((resolve) => {
    setTimeout(resolve, ms);
  });
}
run();